import { gql, useMutation } from "@apollo/client";
import * as React from "react";
import { GET_MY_TODOS } from "./TodoPrivateList";

const INSERT_TODO = gql`
  mutation ($title: String!, $isPublic: Boolean!) {
    insert_todos(objects: { title: $title, is_public: $isPublic }) {
      affected_rows
      returning {
        user {
          id
          name
        }
        is_public
        created_at
        user_id
        id
        title
        is_completed
      }
    }
  }
`;

const TodoInput = ({ isPublic = false }) => {
  const [todoInput, setTodoInput] = React.useState("");
  const [addTodo, { data, loading, error }] = useMutation(INSERT_TODO);

  return (
    <>
      {error}
      {loading && "Loading..."}
      <form
        className="formInput"
        onSubmit={(e) => {
          e.preventDefault();
          setTodoInput("");
          addTodo({
            variables: {
              title: todoInput,
              isPublic,
            },
            update(cache, { data }) {
              if (isPublic || !data) {
                return null;
              }
              const getExistingTodos: any = cache.readQuery({
                query: GET_MY_TODOS,
              });
              const existingTodos = getExistingTodos
                ? getExistingTodos.todos
                : [];
              const newTodo = data.insert_todos!.returning[0];
              cache.writeQuery({
                query: GET_MY_TODOS,
                data: { todos: [newTodo, ...existingTodos] },
              });
            },
          });
        }}
      >
        <input
          disabled={loading}
          className="input"
          placeholder="What needs to be done?"
          value={todoInput}
          onChange={(e) => setTodoInput(e.target.value)}
        />
        <i className="inputMarker fa fa-angle-right" />
      </form>
    </>
  );
};

export default TodoInput;
