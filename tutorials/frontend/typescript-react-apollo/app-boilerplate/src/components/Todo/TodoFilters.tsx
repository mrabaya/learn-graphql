/* eslint-disable jsx-a11y/anchor-is-valid */
import { gql } from "@apollo/client";
import * as React from "react";
import { GetMyTodosQuery } from "../../generated/graphql";

interface filterResults {
  (filter: string): void;
}

interface TodoFiltersArgs {
  todos: GetMyTodosQuery["todos"];
  currentFilter: string;
  filterResultsFn: filterResults;
  clearCompletedFn: VoidFunction;
}

const TodoFilters = ({
  todos,
  currentFilter,
  filterResultsFn,
  clearCompletedFn,
}: TodoFiltersArgs) => {
  const filterResultsHandler: filterResults = (filter: string) => {
    // enum type def
    filterResultsFn(filter);
  };
  const activeTodos = todos.filter((todo) => todo.is_completed !== true);
  const completedTodos = todos.filter((todo) => todo.is_completed === true);

  // The clear completed button if these are personal todos
  const clearCompletedButton = (
    <button
      onClick={clearCompletedFn}
      className="clearComp"
      disabled={completedTodos.length === 0}
    >
      Clear completed ({completedTodos.length})
    </button>
  );

  let itemCount = todos.length;
  if (currentFilter === "active") {
    itemCount = activeTodos.length;
  } else if (currentFilter === "completed") {
    itemCount = todos.length - activeTodos.length;
  }

  return (
    <div className="footerList">
      <span>
        {" "}
        {itemCount} item{itemCount !== 1 ? "s" : ""}
      </span>

      <ul>
        <li
          onClick={(e) => {
            filterResultsHandler("all");
          }}
        >
          <a className={currentFilter === "all" ? "selected" : ""}>All</a>
        </li>

        <li
          onClick={(e) => {
            filterResultsHandler("active");
          }}
        >
          <a className={currentFilter === "active" ? "selected" : ""}>Active</a>
        </li>

        <li
          onClick={(e) => {
            filterResultsHandler("completed");
          }}
        >
          <a className={currentFilter === "completed" ? "selected" : ""}>
            Completed
          </a>
        </li>
      </ul>

      {clearCompletedButton}
    </div>
  );
};

export default TodoFilters;
