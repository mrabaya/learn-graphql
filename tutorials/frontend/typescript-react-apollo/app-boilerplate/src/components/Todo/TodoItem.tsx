import { gql, useMutation } from "@apollo/client";
import * as React from "react";
import { GET_MY_TODOS } from "./TodoPrivateList";
import loadingIndicator from "../Auth/loading.svg";
import {
  GetMyTodosQuery,
  Todos,
  Mutation_RootInsert_TodosArgs,
  Mutation_RootDelete_TodosArgs,
  Mutation_RootUpdate_TodosArgs,
} from "../../generated/graphql";

interface TodoItemType {
  index: number;
  todo: Pick<Todos, "id" | "title" | "is_completed">;
}

export const TOGGLE_TODO = gql`
  mutation toggleTodo($id: Int!, $isCompleted: Boolean!) {
    update_todos(
      where: { id: { _eq: $id } }
      _set: { is_completed: $isCompleted }
    ) {
      affected_rows
      returning {
        id
        title
        is_completed
      }
    }
  }
`;

const REMOVE_TODO = gql`
  mutation ($id: Int!) {
    delete_todos(where: { id: { _eq: $id } }) {
      affected_rows
    }
  }
`;

const TodoItem = ({ index, todo }: TodoItemType) => {
  const [deleteTodo] = useMutation<Mutation_RootDelete_TodosArgs>(REMOVE_TODO, {
    update(cache, { data }) {
      const existingTodos = cache.readQuery<GetMyTodosQuery>({
        query: GET_MY_TODOS,
      });
      const updatedTodos = existingTodos!.todos.filter(
        (x: Pick<Todos, "id" | "title" | "is_completed">, i: number) =>
          i !== index
      );
      cache.writeQuery<GetMyTodosQuery>({
        query: GET_MY_TODOS,
        data: {
          todos: updatedTodos,
        },
      });
    },
  });
  const [updateTodo] = useMutation(TOGGLE_TODO, {
    update(cache, { data }) {
      const existingTodos = cache.readQuery<GetMyTodosQuery>({
        query: GET_MY_TODOS,
      });
      const updatedTodos = existingTodos!.todos.map(
        (t: Pick<Todos, "id" | "title" | "is_completed">) => {
          if (t.id === todo.id) {
            return {
              ...t,
              ...data!.update_todos!.returning[0],
            };
          } else {
            return t;
          }
        }
      );
      cache.writeQuery<GetMyTodosQuery>({
        query: GET_MY_TODOS,
        data: {
          todos: updatedTodos,
        },
      });
    },
  });

  const removeTodo = (e: React.MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();
    deleteTodo({
      variables: {
        id: todo.id,
      },
    });
  };

  const toggleTodo = () => {
    updateTodo({
      variables: {
        id: todo.id,
        isCompleted: !todo.is_completed,
      },
      optimisticResponse: {
        __typename: "mutation_root",
        update_todos: {
          __typename: "todos_mutation_response",
          affected_rows: 1,
          returning: [
            {
              __typename: "todos",
              id: todo.id,
              title: todo.title,
              is_completed: !todo.is_completed,
            },
          ],
        },
      },
    });
  };

  return (
    <li key={index}>
      <div className="view">
        <div className="round">
          <input
            checked={todo.is_completed}
            type="checkbox"
            id={todo.id!.toString()}
            onChange={() => toggleTodo()}
          />
          <label htmlFor={todo.id!.toString()} />
        </div>
      </div>

      <div className={"labelContent" + (todo.is_completed ? " completed" : "")}>
        <div>{todo.title}</div>
      </div>

      {/* {!loading ? ( */}
      <button className="closeBtn" onClick={(e) => removeTodo(e)}>
        x
      </button>
      {/* ) : (
        <img src={loadingIndicator} width="25" alt="loading" />
      )} */}
    </li>
  );
};

export default TodoItem;
