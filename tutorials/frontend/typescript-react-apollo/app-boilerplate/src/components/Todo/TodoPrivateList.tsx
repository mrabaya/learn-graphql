import React, { Fragment, useState } from "react";
import TodoItem from "./TodoItem";
import TodoFilters from "./TodoFilters";
import { gql, useMutation, useQuery } from "@apollo/client";
import {
  GetMyTodosQuery,
  Todos,
  Mutation_RootDelete_TodosArgs,
} from "../../generated/graphql";

export const CLEAR_COMPLETED = gql`
  mutation clearCompleted($isCompleted: Boolean!, $isPublic: Boolean!) {
    delete_todos(
      where: {
        is_completed: { _eq: $isCompleted }
        is_public: { _eq: $isPublic }
      }
    ) {
      affected_rows
    }
  }
`;

export const GET_MY_TODOS = gql`
  query getMyTodos {
    todos(
      where: { is_public: { _eq: false } }
      order_by: { created_at: desc }
    ) {
      id
      title
      is_completed
    }
  }
`;

const TodoPrivateList = () => {
  const [filter, setFilter] = useState<string>("all");
  const { loading, error, data } = useQuery<GetMyTodosQuery>(GET_MY_TODOS);
  const [clearCompletedTodos] = useMutation<Mutation_RootDelete_TodosArgs>(
    CLEAR_COMPLETED,
    {
      update(cache, { data }) {
        const existingTodos = cache.readQuery<GetMyTodosQuery>({
          query: GET_MY_TODOS,
        });
        const updatedTodo = existingTodos!.todos.filter(
          (item) => item.is_completed === false
        );
        cache.writeQuery<GetMyTodosQuery>({
          query: GET_MY_TODOS,
          data: {
            todos: updatedTodo,
          },
        });
      },
    }
  );

  const filterResults = (filter: string): void => {
    setFilter(filter);
  };

  const clearCompleted = () => {
    clearCompletedTodos({
      variables: {
        isCompleted: true,
        isPublic: false,
      },
    });
  };

  if (loading) {
    return <div>Loading...</div>;
  }
  if (error || !data) {
    return <div>Error...</div>;
  }

  let filteredTodos = data.todos;
  if (filter === "active") {
    filteredTodos = data.todos.filter(
      (todo: Pick<Todos, "id" | "title" | "is_completed">) =>
        todo.is_completed !== true
    );
  } else if (filter === "completed") {
    filteredTodos = data.todos.filter(
      (todo: Pick<Todos, "id" | "title" | "is_completed">) =>
        todo.is_completed === true
    );
  }

  const todoList = filteredTodos.map(
    (todo: Pick<Todos, "id" | "title" | "is_completed">, index: number) => (
      <TodoItem key={"item" + index} index={index} todo={todo} />
    )
  );

  return (
    <Fragment>
      <div className="todoListWrapper">
        <ul>{todoList}</ul>
      </div>

      <TodoFilters
        todos={filteredTodos}
        currentFilter={filter}
        filterResultsFn={filterResults}
        clearCompletedFn={clearCompleted}
      />
    </Fragment>
  );
};

export default TodoPrivateList;
