import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import { useCallback, useEffect, useMemo } from "react";
import { WebSocketLink } from "@apollo/client/link/ws";

const useApollo = (authToken: string) => {
  const client = useMemo(() => {
    return new ApolloClient({
      link: new WebSocketLink({
        uri: "wss://hasura.io/learn/graphql",
        options: {
          reconnect: true,
          connectionParams: {
            headers: {
              Authorization: `Bearer ${authToken}`,
            },
          },
        },
      }),
      cache: new InMemoryCache(),
    });
  }, []);
  return { client };
};
export default useApollo;
